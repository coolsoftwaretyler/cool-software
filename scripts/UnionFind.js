function UnionFind(size) {
  // Initialize the root array with each element pointing to itself.
  this.root = new Array(size)
  for (let i = 0; i < size; i++) {
    this.root[i] = i
  }
}

UnionFind.prototype.find = function (x) {
  return this.root[x]
}

UnionFind.prototype.union = function (x, y) {
  // First, find the roots of x and y
  const rootX = this.find(x)
  const rootY = this.find(y)

  // If the roots are the same, there is no need to union.
  // Otherwise, change every root for rootY to rootX
  if (rootX !== rootY) {
    for (let i = 0; i < this.root.length; i++) {
      if (this.root[i] === rootY) {
        this.root[i] = rootX
      }
    }
  }
}

module.exports = UnionFind
