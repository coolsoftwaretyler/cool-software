/* eslint-disable no-console */
const fs = require('fs')
const path = require('path')

newProblem()

function newProblem() {
  const filePath = generateFilePath()
  const content = generateContent()
  fs.writeFile(filePath, content, { flag: 'w+' }, (err) => {
    if (err) return console.log(err)
    console.log(`Created ${filePath}`)
  })
}

function generateFilePath() {
  return path.join(__dirname, '..', 'content', 'leetcode-yaml', `_newPost.yml`)
}

function generateContent() {
  return `name:
link:
first_attempt:
last_review:
difficulty:
patterns: []
full_answer:
reflection:
todo:

`
}
