function Heap(variant) {
  // Set up an array to represent the heap
  this.heapArray = []
  // Keep track of the variant provided, either `max` or `min
  this.variant = variant
}

/**
 * The following index helpers assume a 0-indexed array,
 * which changes the offset values from what most Heap documentation says
 **/
Heap.prototype.parentIndex = function (index) {
  return Math.floor((index - 1) / 2)
}

Heap.prototype.leftChildIndex = function (index) {
  return index * 2 + 1
}

Heap.prototype.rightChildIndex = function (index) {
  return index * 2 + 2
}

/**
 * This helper function will return a boolean value based on a greater/less than comparison,
 * which we switch based on the variant provided in the construction of the object.
 * It takes two parameters, a and b, and returns either a > b or a < b, for max heaps/min heaps, respectively.
 **/
Heap.prototype.compare = function (a, b) {
  if (this.variant === 'max') {
    return a > b
  } else {
    return a < b
  }
}

/**
 * When we fix the heap, we may need to swap nodes. This function takes two positions,
 * and swaps the values at those positions
 **/
Heap.prototype.swap = function (position1, position2) {
  const valueAtPosition1 = this.heapArray[position1]
  const valueAtPosition2 = this.heapArray[position2]

  this.heapArray[position1] = valueAtPosition2
  this.heapArray[position2] = valueAtPosition1
}

/**
 * This helper function returns the stringified value of the heapArray
 */
Heap.prototype.print = function () {
  return this.heapArray.toString()
}

/**
 * This helper function returns the top of the heap without modifying it
 */
Heap.prototype.peek = function () {
  return this.heapArray[0]
}

/**
 * This helper function returns the size of the heapArray
 */
Heap.prototype.size = function () {
  return this.heapArray.length
}

Heap.prototype.insert = function (value) {
  // We're going to start by inserting the value at the end of the array.
  // We'll also want to know that index since we're going to use it to find the parent node.
  // So start by getting this index, which is just the heapArray length
  let currentIndex = this.heapArray.length

  // Then add the value to the heapArray at that spot
  this.heapArray[currentIndex] = value

  // Now we need to fix the heap, which we'll do from the "bottom" up, meaning
  // we start at the new index, and check its parent node, and so on and so forth.
  let parentIndex = this.parentIndex(currentIndex)

  // Check the values of the two nodes
  const currentValue = this.heapArray[currentIndex]
  let parentValue = this.heapArray[parentIndex]

  // The compare function will return currentValue > parentValue for a max heap,
  // or currentValue < parentValue for a min heap. Either of those conditions is INCORRECT,
  // which means we need to "fix" the heap.
  // Only do this while the currentIndex is greater than 0. If it's at 0, then it's the root node,
  // and we have nothing to check.
  while (currentIndex > 0 && this.compare(currentValue, parentValue)) {
    // First, swap our current node with its parent node
    this.swap(currentIndex, parentIndex)

    // Then, update the currentIndex to be the parentIndex
    currentIndex = parentIndex

    // Find the parent index of the new currentIndex
    parentIndex = this.parentIndex(currentIndex)
    // Get the value of our new parent
    parentValue = this.heapArray[parentIndex]
  }
}

Heap.prototype.pop = function () {
  // First, get the value of the root node
  const root = this.heapArray[0]
  // Then pop off the end value and overwrite the root node
  const end = this.heapArray.pop()
  this.heapArray[0] = end

  // Start from the index of the root node and fix the heap in the "top" down direction
  let index = 0

  // We'll want to know the length of the heapArray,
  // so we can check for when child indices go out of bounds.
  const length = this.heapArray.length

  // The current value for comparison is the value of the new root node.
  const current = this.heapArray[0]

  // Loop infinitely, until we hit criteria to stop.
  while (true) {
    // Calculate the indices of the left and right children
    const leftChildIndex = this.leftChildIndex(index)
    const rightChildIndex = this.rightChildIndex(index)

    // Set up placeholder variables for left/right child values
    let leftChild, rightChild

    // Initialize a "swap" variable, which will indicate which index, if any, we should swap with the current index.
    let swap = null

    // Check if the leftChildIndex is within the bounds of the heapArray
    if (leftChildIndex < length) {
      // If so, the leftChild value is the value at the leftChildIndex
      leftChild = this.heapArray[leftChildIndex]

      // Check if the leftChild value violates the heap properties. If so, let's consider swapping current/left child.
      if (this.compare(leftChild, current)) swap = leftChildIndex
    }

    // Check if the rightChildIndex is within the bounds of the heapArray
    if (rightChildIndex < length) {
      // If so, the rightChild value is the value at the rightChildIndex
      rightChild = this.heapArray[rightChildIndex]

      // If we've decided not to swap, but the rightChild value violates the heap properties, let's swap right/current.
      // Or, if we didn't decide to swap on the left child, but the rightChild value violates the heap properties, let's swap right/current.
      if (
        (swap === null && this.compare(rightChild, current)) ||
        (swap !== null && this.compare(rightChild, leftChild))
      )
        swap = rightChildIndex
    }

    // If we never found a swap variable, we should break, because the heap is intact.
    if (swap === null) break

    // Otherwise, we found a swap variable, so let's swap the current index with the swap variable.
    this.swap(index, swap)

    // Keep going, but this time use the swap variable as the new index.
    index = swap
  }

  return root
}

module.exports = Heap
