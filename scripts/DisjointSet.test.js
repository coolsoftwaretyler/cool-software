/* eslint-disable no-console */
const UnionFind = require('./UnionFind')

const uf = new UnionFind(7)

console.log('Created new UnionFind')
console.log('The root of 1 is initially: ', uf.find(1))
console.log('Connect 0 and 1')
uf.union(0, 1)
console.log('The root of 1 is now: ', uf.find(1))
