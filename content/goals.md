# Goals

Running timeline of how I feel about my goals and what they look like:

## 2022-04-09

It's hard to say precisely what I want out of this. Here's an initial thought: 

1. Get good enough at LeetCode to do hard problems and LC competitions.
1. Use LC skills to pivot to a new language suitable for competitive programming
1. Start competitive programming
1. ???

To start on these steps, I want to start with the [neetcode.io](https://neetcode.io/) list of 150 problems. I want to do one or two per day, and alternate old problems (spaced repetition) with new problems.

It's hard to see much past that. Last time I did that, I did about 60 days worth of problems. This time around, let's see if I can do 3-4 months before stopping. Ideally I would do this forever, but let's at least set a goal of 90 days. I can use the new LC streak feature to track this.

## 2022-05-05

Just a quick update: I'm scoping down to the Blind 75 first so I can approach all the topics, then I'm going to expand to the Neetcode 150 (and include Blind 75 review). It'll be nice to have a smaller intermediate goal of 75 problems, rather than the bigger 150, which is a super-set of Blind 75 anyways. 

As of today, I'm at about 35/75 on Blind 75, and 46/150 Neetcode.
