---
title: Longest Repeating Character Replacement
description: You are given a string s and an integer k. You can choose any character of the string and change it to any other uppercase English character. You can perform this operation at most k times.
slug: longest-repeating-character-replacement
link: https://leetcode.com/problems/longest-repeating-character-replacement/
---

You are given a string s and an integer k. You can choose any character of the string and change it to any other uppercase English character. You can perform this operation at most k times.

Return the length of the longest substring containing the same letter you can get after performing the above operations.

 

Example 1:

Input: s = "ABAB", k = 2
Output: 4
Explanation: Replace the two 'A's with two 'B's or vice versa.
Example 2:

Input: s = "AABABBA", k = 1
Output: 4
Explanation: Replace the one 'A' in the middle with 'B' and form "AABBBBA".
The substring "BBBB" has the longest repeating letters, which is 4.
 

Constraints:

1 <= s.length <= 105
s consists of only uppercase English letters.
0 <= k <= s.length

## Solution

This problem has always been a real challenge for me. It's a pretty classic sliding window problem, and there are two versions of the solution. Here's the crux of it, though:

1. You have to put together what a "valid" window would be. In this case, the key insight is that any set of characters in the string could fit the requirements if: `Length of the substring - count of the most frequent character in substring <= k`. This basically means, for any given substring in the string, find the most frequent character (since it's the best candidate to use in substitutions of other letters). Then see how many letters in that substring are *not* the most frequent character. If that number is less than or equal to our allowed number of substitutions, this window *could* be a candidate for longest window. 
1. Finding the `count of the most frequent character in substring` is where we can make two choices in our algorithm. In one case, we can recalculate the most frequent letter we've seen. Since the problem says the input is made of only uppercase English letters, this means our runtime would be O(26n), which reduces to O(n). But there's a trick: we don't have to recalculate this every time. If we reduce the size of a window, there's no way that it'll be the *longest* window. So we can just keep track of the frequency of the most frequent character we've ever seen, and use that in our calculations.

That second point is a little tricky, so you'll want to pay attention to the inline code comments about it. I had to do this problem about four or five times to really *see* why that works. 

With those insights, we can piece together our algorithm, which is:

1. Create a window, and grow it in the right-hand direction.
1. Every time you move the right side of the window up by one, keep track of the frequency of that letter that you've seen so far.
1. Then, either keep track of the most frequent character you've seen, or move on to the next step
1. Check if `Length of the substring - count of the most frequent character in substring <= k`. If it is, log this as a candidate for longest window, and keep incrementing the right side.
1. If this is invalid, i.e. `Length of the substring - count of the most frequent character in substring > k`, we want to increment the left pointer until it becomes valid. 
1. If you're just keeping track of the most frequent character, all you have to do is decrement the frequency of the old left window character.
1. If you're doing the O(26n) solution, you'll need to decrement that letter's frequency, and recalculate the most frequent letter (by checking some hashmap of frequencies, which may be as large as 26 items, and finding the most frequent element).
1. After that, return the longest valid window size.

Here's how to do the O(n) solution, in JavaScript, along with some code comments that explain our decisions, and why we don't need to recalculate the most frequent character. 

```js
/**
 * @param {string} s
 * @param {number} k
 * @return {number}
 */
var characterReplacement = function(s, k) {
    // We're going to track a few things here: 
    // First, set up a hashmap to track the frequency of letters
    const frequencies = {}
    // Then, set up a variable to track the most frequent letter we've seen.
    let highestFrequency = 0
    // Finally, set up a variable to track the size of the longest valid window we encounter.
    let longest = 0
    
    // Then we'll start a window with a left and right side pointer, both beginning at position 0,
    // because we want to grow our window as much as possible, and shrink when we have to.
    let left = 0
    let right = 0
    
    // Then we will increment the right pointer until it hits the end of the input string s
    while (right < s.length) {
        // For every character the right side of the window encounters,
        // we either add it to the frequencies hashmap with a frequency of 1,
        // or we increment its existing value
        const rightCharacter = s.charAt(right)
        frequencies[rightCharacter] = frequencies[rightCharacter] + 1 || 1
        
        // Then we check if this newly encountered character is also the most frequent
        // we have ever seen in the string (even outside of the current window)
        highestFrequency = Math.max(highestFrequency, frequencies[rightCharacter])
        
        // A window is valid if the length of the window,
        // minus the count of the most frequent character we've ever seen,
        // is less than or equal to k. 
        // That means if the current window has the most frequent character in it, 
        // and we did k replacements of the other letters in that window,
        // we would have enough k replacements to make the entire window that most frequent letter.
        // 
        // If the current window is not valid, we want to increment the left pointer until we get
        // to a valid window
        //
        // Each time we do this, decrement the frequency of the character we're truncating,
        // since it's no longer part of the window.
        // 
        // We do not, however, have to update highestFrequency, 
        // because we'll only get a longer valid window when we encounter a letter that is 
        // more frequent in its window than the last highestFrequency count was.
        // In all other cases, even when we find valid windows, they will necessarily be shorter
        // than the last time the highestFrequency gave us a valid result.
        while ((right - left + 1) - highestFrequency > k) {
            const leftCharacter = s.charAt(left)
            frequencies[leftCharacter] -= 1
            left++
        }
        
        // Once we have a valid window, check if it's longer than the previous longest valid window,
        // and store that in our longest variable.
        longest = Math.max(longest, right - left + 1)
        
        // Finally, increment the right pointer to shift the window to the right
        right++
    }
    
    // Return the longest valid window we've seen
    return longest
};
```
