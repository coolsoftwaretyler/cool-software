---
title: Group Anagrams
description: Given an array of strings strs, group the anagrams together. You can return the answer in any order.
slug: group-anagrams
--- 

## Description

[Link](https://leetcode.com/problems/group-anagrams/)

Given an array of strings strs, group the anagrams together. You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.

**Example 1:**

- Input: strs = ["eat","tea","tan","ate","nat","bat"]
- Output: [["bat"],["nat","tan"],["ate","eat","tea"]]

**Example 2:**

- Input: strs = [""]
- Output: [[""]]

**Example 3:**

- Input: strs = ["a"]
- Output: [["a"]]

**Constraints:**

- 1 <= strs.length <= 104
- 0 <= strs[i].length <= 100
- strs[i] consists of lowercase English letters.

## Solution

We can solve this problem using a hash map. The problem gives us a few hints about that: 

1. This is an array based problem with strings
1. We can return the answer in any order

Arrays and strings lend themselves well to solutions that use hash maps, two pointers, and sliding windows. But since we don't need to really compute windows, and since we need to evaluate the whole array, hash maps should be the tool we reach for in this case. 

The trick here is to think of what we'll use for the *keys* in our hashmap. If you look at the constraints, you'll see that all of the words consist of lowercase English letters. That's another hint. 

If we consider what an "anagram" is - we get the first clue about the keys for our hashmap. Any anagram, by definition, must have the same number of the same type of letters. 

So taking "tea" and "tea", we know that both of those words have one t, one e, and one a. That makes them an anagram. 

We can use that information to *encode* our words, and use that encoding as our hash map values. 

Back to "tea" and "eat", consider an array of length 26, which represents all lowercase english letters: 

```js
const encodingArray = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
```

If we went through both of these strings and incremented the value of the ocrresponding letter position in that array, we would end up with arrays that look the same: 

```
[1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
```

And then if we joined that array together with a delineating character, such as `#`, we would get `1#0#0#0#1#0#0#0#0#0#0#0#0#0#0#0#0#0#0#1#0#0#0#0#0#0` 

What's nifty about that is, since it's a string, we can use it as a key in a JavaScript hashmap (or object). That's the crux of the solution here. 

Now that we know how to encode anagrams, we can use that encoding to keep track of them in linear time. Here's how: 

1. Set up a hash to keep track of our results
1. Go through each string in `strs`
1. Encode the string
1. If the encoding of that string doesn't exist in the hash map, push the raw string into an array and set it up as the value of that hash key/value pair.
1. If the encoding of that string already exists in the hash map, push the raw string into the array for that encoding string.
1. Go through the hash map and gather those values, pushing them into a final result array
1. Return the array. 

And here's the code: 

```js
/**
 * @param {string[]} strs
 * @return {string[][]}
 */
var groupAnagrams = function(strs) {
    
    // Create an encoding function so we can get valid keys for our hashmap
    const encode = (string) => {
        // Set up an array to correspond to the lowercase letters a-z
        const letters = new Array(26).fill(0)
        
        // The character code for `a` is `97`, and the character code for `z` is `122`
        // we can use that to increment the array values. If we subtract 97 from the character code,
        // it will give us the relative array position. So `a` becomes `0` and `z` becomes `25`
        for (let i=0; i<string.length; i++) {
            const relativePosition = string.charCodeAt(i) - 97
            letters[relativePosition] += 1
        }
        
        // Join the array as a string and return it
        // Use an octothorpe to delineate between letter counts
        return letters.join('#')
    }

    // Set up a hash to keep track of the results
    const hashmap = {}
    
    // Go through each string in `strs`
    for (string of strs) {
        // Encode the string
        const encodedValue = encode(string) 
        // If the encoded value does not exist in the hash map, push the string into an array and make it the value.
        // Otherwise, append it to the existing array
        if (!hashmap.hasOwnProperty(encodedValue)) {
            const value = [string]
            hashmap[encodedValue] = value
        } else {
            hashmap[encodedValue].push(string)
        }
    }
    
    // Return all the values from the hashmap, which should be grouped by encoding strings.
    // We can do this in JS with Object.values(someObject)
    
    return Object.values(hashmap)
};
```
