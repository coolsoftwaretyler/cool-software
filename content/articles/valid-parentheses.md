---
title: Valid Parentheses 
description: Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
slug: valid-parentheses
link: https://leetcode.com/problems/valid-parentheses/
---

Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.


An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
 

Example 1:

Input: s = "()"
Output: true
Example 2:

Input: s = "()[]{}"
Output: true
Example 3:

Input: s = "(]"
Output: false
 

Constraints:

1 <= s.length <= 104
s consists of parentheses only '()[]{}'.

## Solution

We can solve this problem using a stack data structure. It's not so much of a pattern that you have to recognize, but an application of an appropriate data type. 

In a given input string, since we only have the characters: `()[]{}` as possible inputs, there are two scenarios for us as we look down the string: 

1. If the character at any given position is an opening character, `(`, `[`, or `{`, it's always valid. We can open up a parenthesis at any point (assuming the string hasn't already been invalidated)
1. If the character at a given position is a closing character, `)`, `]`], or `}`, we need to check if it's a valid closing character. 

To check if a closing character valid, we want to know what the last opening character was, and ensure that it corresponds to this closing character.

Keeping track of the last of something is a good use for a stack, which uses "last in first out" as a strategy to insert and remove items.

To use a stack for this solution, we can do the following: 

1. Initialize an empty stack
1. Initialize a hashmap that maps closing characters with their corresponding valid opening character
1. Iterate through the string characters. For every opening character we run into, push it into the stack.
1. For every closing character, pop the stack and check that it matches the closing character's valid opener. If it does, move on. If it doesn't, return false, because this is an invalid string. 
1. Finally, make sure the stack is empty, because if it isn't, it means we have an un-closed opener at some point. 

Here's how to write that in JavaScript: 

```js
/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function(s) {
    // Initialize an empty stack, 
    // in JavaScript, we can use an array for that.
    const stack = []
    
    // Set up a key/value store (hashmap, object, etc.) that tells us 
    // which characters are valid openers for each closer:
    const closersAndTheirOpenings = {
        ')': '(',
        ']': '[',
        '}': '{'
    }
    
    // Iterate through the string
    for (let i=0; i<s.length; i++) {
        const character = s.charAt(i)
        
        // Check if this character is a key in the closersAndTheirOpenings object:
        if (closersAndTheirOpenings.hasOwnProperty(character)) {
            // If so, pop the stack
            const poppedCharacter = stack.pop()
            
            // Check if the poppedCharacter is the opening for the current closing character.
            // If it isn't, then break the loop and return false, because this is an invalid string.
            if (poppedCharacter !== closersAndTheirOpenings[character]) {
                return false
            }
        } else {
            // If this character is not a key in closersAndTheirOpenings,
            // then it is an opening character. In which case, we just pop it onto the stack
            stack.push(character)
        }
        
    }
    
    
    // If we made it through the string without returning false,
    // this might be a valid string.
    // 
    // We need to check that the stack is empty. If it's empty, we have used 
    // and closed every opener. If it is not empty (length > 0), then we have 
    // an unclosed opener, which is invalid.
    return stack.length === 0
};
```
