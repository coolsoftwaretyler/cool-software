---
title: Two Sum 
description: Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
slug: two-sum
---

## Description 

Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

 

Example 1:

Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:

Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:

Input: nums = [3,3], target = 6
Output: [0,1]
 

Constraints:

2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
Only one valid answer exists.
 

Follow-up: Can you come up with an algorithm that is less than O(n2) time complexity?

## Solution 

This problem can be solved in linear time using a hashmap. There are two clues that suggest this two us: 

1. We're looking for a linear time solution in an array
1. The solution conditions have to do with combinations of two elements of the array.

A linear solution-time solution that involves two elements in the array is likely to lend itself well to a hashmap solution, where we can use our hashmap to keep track of information about each element in the array, and look it up in constant time. 

In this case, two numbers in the array will add up to the target number if: 

`target - number1 = number2`, or `target - number2 = number1`.

So we can solve this in linear time, in a single pass, by iterating through the array and performing to wthings at each item: 

1. Check a hashmap if we've seen `target - number` before, and if so, return the current index and the index of where we saw the difference.
1. If we haven't seen the difference, keep track of it by storing it in the hashmap as a key, where its value is the index at which we saw it.

Here's the code to do that:

## Code

```js
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
    // Set up a hashmap to keep track of key/value pairs,
    // where the key is nums[i], and the value is i.
    // We'll use this to look up numbers that exist in the array in constant time,
    // which is useful since we were told that each input has exactly one solution.
    const hashmap = {}
    
    // Iterate through the numbers in the array,
    for (let i=0; i<nums.length; i++) {
        // Check the difference between the target numbers, and nums[i]
        const difference = target - nums[i]
        
        // Since we know the difference between target and nums[i],
        // we can check to see if we've seen it already, by checking if hashmap[difference] exists.
        //
        // If we have seen the difference before, we return the current index, 
        // and the index at which we saw the difference.
        if (hashmap.hasOwnProperty(difference)) {
            return [i, hashmap[difference]]
        }
        
        // Otherwise, store the current number in the hashmap, 
        // along with its index
        hashmap[nums[i]] = i
    }
};
```
