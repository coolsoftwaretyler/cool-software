---
title: Valid Anagram 
description: Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.
slug: valid-anagram
---

Given two strings s and t, return true if t is an anagram of s, and false otherwise.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.


Example 1:

Input: s = "anagram", t = "nagaram"
Output: true
Example 2:

Input: s = "rat", t = "car"
Output: false
 

Constraints:

1 <= s.length, t.length <= 5 * 104
s and t consist of lowercase English letters.


## Solution

Fundamentally, if we want to determine if two strings are anagrams of one another, we need to verify that they use precisely the same number of the same letters. 

The constraints here tell us that the input strings will only use lowercase english letters, which further reduces our consideration. 

For problems that ask us to make assertions about the frequency of letters or elements in an array or string, we should start by considering a hashmap. 

Hashmap data structures are great for these kinds of problems, because they allow us to store information about arrays and strings in a way that allows for constant-time lookup. 

In this problem, we'll use a hash map to count the number of occurrences of each letter in both strings. 

Once we've counted the occurences, we'll compare our character counts between both strings. If anything differs, we'll return `false`. Otherwise, we'll return `true`. 

Here's how we can accomplish that in JavaScript.

## Code

```js
/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isAnagram = function(s, t) {
  const sMap = {a: 0, b: 0, c: 0, d: 0, e: 0, f: 0, g: 0, h: 0, i: 0, j: 0, k: 0, l: 0, m: 0, n: 0, o: 0, p: 0, q: 0, r: 0, s: 0, t: 0, u: 0, v: 0, w: 0, x: 0, y: 0, z: 0}

  const tMap = {a: 0, b: 0, c: 0, d: 0, e: 0, f: 0, g: 0, h: 0, i: 0, j: 0, k: 0, l: 0, m: 0, n: 0, o: 0, p: 0, q: 0, r: 0, s: 0, t: 0, u: 0, v: 0, w: 0, x: 0, y: 0, z: 0}

  // Loop through string s and count the occurence of each character, storing the count in sMap
  for (let i=0; i<s.length; i++) {
    const character = s.charAt(i)
    sMap[character] += 1
  }

  // Loop through string t and count the occurence of each character, storing the count in tMap
  for (let i=0; i<t.length; i++) {
    const character = t.charAt(i)
    tMap[character] += 1
  }

  // Then check every key in sMap, and confirm its value equals the corresponding key in tMap
  const keys = Object.keys(sMap)

  for (let i=0; i<keys.length; i++) {
    const key = keys[i]

    if (sMap[key] !== tMap[key]) {
      return false
    }
  }

  // If we got through that loop without returning false, 
  // return true, because the strings must be anagrams
  return true
};
```

## Optimization

We can optimize this solution in a few ways: 

1. Two strings of different length can't possibly be an anagram. So instead of going through this entire algorithm, we can check the input strings' length at the beginning of the function. If they don't match, we can return `false` early.
1. We can use a single hashmap instead of two. Since we want the number of occurrences to be equal, we can start with a single hash map, increment the values as we go through string `s`, and decrement the values as we go through string `t`. Then, instead of checking for equality across `sMap` and `tMap`, we can verify all the values in the map are `0`. 

```js
/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isAnagram = function(s, t) {
  // If the input strings are of different lengths,
  // they can't be anagrams, and we can just return false
  if (s.length !== t.length) {
    return false
  }

  const characters = {a: 0, b: 0, c: 0, d: 0, e: 0, f: 0, g: 0, h: 0, i: 0, j: 0, k: 0, l: 0, m: 0, n: 0, o: 0, p: 0, q: 0, r: 0, s: 0, t: 0, u: 0, v: 0, w: 0, x: 0, y: 0, z: 0}

  // Loop through string s and count the occurence of each character by incrementing its count in the characters hashmap.
  for (let i=0; i<s.length; i++) {
    const character = s.charAt(i)
    characters[character] += 1
  }

  // Loop through string t and decrement the count for each character in the characters hashmap.
  for (let i=0; i<t.length; i++) {
    const character = t.charAt(i)
    characters[character] -= 1
  }

  // Then check every key in characters, and confirm it has been zeroed out.
  const keys = Object.keys(characters)

  for (let i=0; i<keys.length; i++) {
    const key = keys[i]

    if (characters[key] !== 0) {
      return false
    }
  }

  // If we got through that loop without returning false, 
  // return true, because the strings must be anagrams
  return true
};
```

## Runtime

All in all, this solution only requires us to loop through string `s` once, and string `t` once. If the length of `s` and `t` are equal, and that length is our `N`, this runs in O(2N), or simplified to O(N).
