---
title: Longest Substring Without Repeating Characters
description: Given a string s, find the length of the longest substring without repeating characters.
slug: longest-substring-without-repeating-characters
link: https://leetcode.com/problems/longest-substring-without-repeating-characters/
---
Given a string s, find the length of the longest substring without repeating characters.

 

Example 1:

Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.
Example 2:

Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
Example 3:

Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
 

Constraints:

0 <= s.length <= 5 * 104
s consists of English letters, digits, symbols and spaces.

## Solution

Since this problem asks us to find a "longest substring" that matches some criteria (no repeating characters), it's a good bet that we'll want to reach for a sliding window approach to it. 

Since we're trying to find the longest, we want to start our window as small as we can, and then grow it as far to the right as possible, always checking whether or not the window is still valid.

When the window becomes invalid, we will want to move the left side of it up until it becomes valid again, and then start over, repeating this growing and shrinking until the right side of the window is at the end of the input string.

A valid window in this case is one where there are no repeating characters. If we want to keep track of characters we've seen before, we probably want to be able to look them up in constant time, so we can use a hash map to do that for us. 

But when we reach an invalid window, how will we know how far to shrink it? That's the real trick here (and it consistently makes me mess up the answer, every time). In order to shrink to a valid window, we'll want to move our left pointer to the position *directly after* the duplicate letter we've just encountered on the right hand side. AND ALSO: we will want to make sure we "forget" the letters we saw on the left side up until then. The substring needs to reset itself, meaning our tracker also needs to reset values that aren't included in the new, valid window.

So to break it down: 

1. Start a window at the beginning of the string.
1. Move the right side of the window towards the right by one index place.
1. Keep track of each letter we encounter with our right-side window. 
1. If we haven't seen a letter before, remember it and its position in our hashmap, where the key is the letter, and the value is its position.
1. If we haven't seen the letter before, after storing it, calculate the window size: `right - left + 1`, and then store it as the maximum if it's the biggest window we've seen.
1. If we *have* seen the letter before, we want to shrink the left side of the window by moving that left pointer to `index of the first reference of the duplicate letter + 1`
1. As we move the left pointer, we want to "clear" or "forget" all the letters and indices we pass, because we're restarting our substring, and we no longer need to "track" that letter.
1. Once the right side of the window gets to the end of the input string, we're done. Return the largest value we've seen.

Here's how to express that in JavaScript: 

```js
/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function(s) {
  // Inputs of length 0 and 1 can just return their input length,
  // because you can't have duplicate letters in an empty string,
  // or a string of size 1.
  if (s.length < 2) {
    return s.length
  }

  // Start two pointers, both at 0
  let left = 0
  let right = 0

  // Set up a hashmap to track the letters we've seen, and their indices
  const letters = {}

  // Initialize a max variable at 1, since that's the minimum length it could possibly be.
  let max = 1

  // Loop until the right side of the window hits the end
  while (right < s.length) {
    // Find the character at the right window index
    const rightChar = s.charAt(right)

    // Check if we've seen it before
    if (!letters.hasOwnProperty(rightChar)) {
      // If not, add it and its index to the letters hashmap
      letters[rightChar] = right

      // Calculate the length of the window
      const length = right - left + 1

      // Keep track of the longest window size we've seen so far.
      max = Math.max(length, max)
    } else {
      // Otherwise, we have seen this letter before.
      // Move the left pointer one position past the location of the last time we saw this character.
      // At each step of the way, "forget" any characters we come across - they are no longer a part of this substring,
      // so we don't need to worry about their duplicates.
      while (left <= letters[rightChar]) {
        const leftChar = s.charAt(left)
        delete letters[leftChar]
        left++
      }

      // Now this character has a new tracked index, the current right pointer of the window.
      letters[rightChar] = right
    }

    // Always move right
    right++
  }

  return max
};
```
