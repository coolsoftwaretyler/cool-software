---
title: Valid Palindrome 
description: Given a string s, return true if it is a palindrome, or false otherwise.
slug: valid-palindrome
link: https://leetcode.com/problems/valid-palindrome/
---

A phrase is a palindrome if, after converting all uppercase letters into lowercase letters and removing all non-alphanumeric characters, it reads the same forward and backward. Alphanumeric characters include letters and numbers.

Given a string s, return true if it is a palindrome, or false otherwise.

 

Example 1:

Input: s = "A man, a plan, a canal: Panama"
Output: true
Explanation: "amanaplanacanalpanama" is a palindrome.
Example 2:

Input: s = "race a car"
Output: false
Explanation: "raceacar" is not a palindrome.
Example 3:

Input: s = " "
Output: true
Explanation: s is an empty string "" after removing non-alphanumeric characters.
Since an empty string reads the same forward and backward, it is a palindrome.
 

Constraints:

1 <= s.length <= 2 * 105
s consists only of printable ASCII characters.


## Solution

For this problem, we want to use the two pointer pattern. The reason we want to do that is because our goal is to analyze a string (or an array of characters) in a particular order, where elements at the beginning of the string must meet some criteria involving elements at the end of the string. 

More specifically, a valid palindrome requires each character at a position to match the character at a position that mirrors it in the second half of the string. 

The problem allows us to strip the input string of non alphanumeric characters, and to set remaining items to lower case, which makes this simpler. 

To figure out if each character has a valid mirrored pair, we can start our two pointers with one at the beginning (position 0), and one at the end (position input.length - 1). Then we increment the first pointer, while we decrement the second pointer, until they cross paths. 

At each iteration, we check if the characters at those positions match. If not, we can return false, because this string can't be a palindrome. If they do, we check the next one.

Once those pointers pass one another, if we haven't returned false, we can return true, because the input must be a valid palindrome based on the rules provided.

Here's how to do that in JavaScript:

```js
/**
 * @param {string} s
 * @return {boolean}
 */
var isPalindrome = function(s) {
    
    // Use replace() method to
    // match and remove all the
    // non-alphanumeric characters,
    // and then run `toLowerCase` on it as well.
    const regex = /[^A-Za-z0-9]/g;
    const formattedString = s.replace(regex, "").toLowerCase()
    
    // Then, use two "pointers", 
    // one starts at the beginning of the formatted string,
    // the other starts at the end:
    let i = 0;
    let j = formattedString.length - 1
    
    // Move the pointers towards each other until they pass one another (as in, i become larger than j).
    // Once they pass, we will have explored both the first half (with i),
    // and the second half (with j)
    while (i < j) {
        // At each position i, check if the character at i matches the character at j.
        // If it doesn't, we can return false - this is not a valid palindrome.
        const charAtI = formattedString.charAt(i)
        const charAtJ = formattedString.charAt(j)
        
        if (charAtI !== charAtJ) {
            return false
        }
        
        // Make sure to increment i, and decrement j
        i += 1
        j -= 1
    }
    
    // If we have looped through the entire formatted string without returning false, 
    // it must be a valid palindrome, so we can return true.
    return true
};
```
