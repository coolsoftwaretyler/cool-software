---
title: Container With Most Water
description: Find two lines that together with the x-axis form a container, such that the container contains the most water.
slug: container-with-most-water 
link: https://leetcode.com/problems/container-with-most-water/
---

You are given an integer array height of length n. There are n vertical lines drawn such that the two endpoints of the ith line are (i, 0) and (i, height[i]).

Find two lines that together with the x-axis form a container, such that the container contains the most water.

Return the maximum amount of water a container can store.

Notice that you may not slant the container.

Input: height = [1,8,6,2,5,4,8,3,7]
Output: 49
Explanation: The above vertical lines are represented by array [1,8,6,2,5,4,8,3,7]. In this case, the max area of water (blue section) the container can contain is 49.
Example 2:

Input: height = [1,1]
Output: 1
 

Constraints:

n == height.length
2 <= n <= 105
0 <= height[i] <= 104

## Solution

This problem is a great medium-level problem for the two pointers pattern, because it really demonstrates a key part of understanding two-pointers solutions: you have to figure out, based on the problem requirements, *where to start the pointers, when to move them, and how they ought to be moved*. 

To calculate the area of the water contained, we want to consider two dimensions: the width, as measured by the x-axis distance between the two vertical lines, and the height, as measured by the *shortest* of the two vertical lines. 

To get the maximum area, we want to either maximize for width or maximize for height. To start, we should consider the case of the widest container, where we use the vertical lines at the left and right end of the array. 

Then we calculate the area between them, using their distance, and the shorter of the two heights. We can store it in some `max` variable, which we will update each and every time we iterate over our area.

Next up is the key to this whole problem: if we were using two pointers to consider the endpoints of this array, which pointer should we move? And how should we move it? The input isn't sorted, so we can't just increment or decrement to increase/decrease the size. But we have one additional piece of information to use: the comparative heights of the vertical lines. 

If we are going to move one of our pointers, the only option we have from the outset is to make the area less wide, since the pointers have to move closer to one another. If we are making the container less wide, the only way we could get a better max is to make it taller. And we are more likely to find a taller container if we keep the tallest vertical line we know of, and move the pointer that points at the shortest line between the two. 

That's a bit of a mouthful, so let me lay it out in sequence: 

1. We start with two pointers at the leftmost and rightmost vertical lines. This is the widest container.
1. We calculate the area and use that as a starting value for our maximum water containment
1. We check which of the vertical lines is shortest, and we move the corresponding pointer closer to the other pointer - that way, even though we are making the container *less wide*, we have a better chance to make the container *more tall*
1. We repeat this process while the left pointer is less than the right pointer, all the while keeping track of the max area we've seen. 
1. We return our tracked maximum.

Here's how to express that in JavaScript: 

```js
/**
 * @param {number[]} height
 * @return {number}
 */
var maxArea = function(height) {
    // Set up a variable to store the max height
    let max = null
    
    // Start with two pointers, one at the beginning, the other at the end
    let left = 0
    let right = height.length - 1
    
    // Loop while the pointers have horizontal space between them
    while (left < right) {
        /**
        * Calculate the area: it's the horizontal distance, 
        * multiplied by the height of the shortest vertical line.
        */
        const area = (right - left) * Math.min(height[right], height[left])
        
        // Update the max if this area is bigger
        max = Math.max(max, area)
        
        // Then we have to choose which pointer to move. 
        // We want preserve the tallest height we can, so if the left is shorter,
        // increment it. 
        // If the right is shorter, decrement it
        if (height[left] < height[right]) {
            left++
        } else {
            right--
        }
    }
    
    // Finally, return our max heigh
    return max
};
```
