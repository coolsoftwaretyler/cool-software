---
title: Contains Duplicate
description: Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.
slug: contains-duplicate
---

Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.

Example 1:

Input: nums = [1,2,3,1]
Output: true
Example 2:

Input: nums = [1,2,3,4]
Output: false
Example 3:

Input: nums = [1,1,1,3,3,4,3,2,4,2]
Output: true
 

Constraints:

1 <= nums.length <= 105
-109 <= nums[i] <= 109

## Solution

The trick to this problem is to use a data structure called a hashmap. A hashmap stores data in sets of key/value pairs, and each value can be looked up by its key in constant time. 

With a hashmap, we can walk through the input array and keep track of how many times we've seen each number.

In this case, the only thing we really care about is whether or not we've seen the number even a single time before. So we can use a hashmap that stores keys that represent each numeral, and uses `true` as the corresponding value.

For each iteration in the array, we check the hashmap and ask if it has an entry for the current numberal. If so, we can break from our loop and return `true`. 

If we get to the end of the array and haven't returned yet, we can return `false`.

## Code

We can accomplish this in JS like so: 

```js
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var containsDuplicate = function(nums) {
    // Use a JS object as a hashmap. 
    // You can accomplish this with a JS set object as well:
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set
    const hashmap = {}
    
    for (let i=0; i<nums.length; i++) {
        // If the hashmap thinks the value of this number is `true`,
        // return `true` from the function - we must have a duplicate.
		    //
		    // If you're using a Set, you would say something like `set.has(nums[i])`
        if (hashmap[nums[i]]) {
            return true
        }
        
        // In the case we haven't seen this number before, we add it to the hashmap,
        // with the value `true`.
		    // If you're using a set, you would use something like `set.add(nums[i])`
        //
        // Since the if statement above returns and breaks the loop,
        // no need for an `else` clause here.
        hashmap[nums[i]] = true
    }
    
    // If we complete the loop above without returning,
    // we never saw a duplicate, so we should return `false` from the function.
    return false
};
```
