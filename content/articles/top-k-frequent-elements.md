---
title: Top K Frequent Elements 
description: Given a string s, return true if it is a palindrome, or false otherwise.
slug: top-k-frequent-elements
link: https://leetcode.com/problems/top-k-frequent-elements/
---
Given an integer array nums and an integer k, return the k most frequent elements. You may return the answer in any order.

Example 1:

Input: nums = [1,1,1,2,2,3], k = 2
Output: [1,2]
Example 2:

Input: nums = [1], k = 1
Output: [1]
 

Constraints:

1 <= nums.length <= 105
k is in the range [1, the number of unique elements in the array].
It is guaranteed that the answer is unique.
 

Follow up: Your algorithm's time complexity must be better than O(n log n), where n is the array's size.

## Solution

There are few different ways to approach this problem, but one of the most efficient in terms of time complexity is by using bucket sort. 

This is a trick that I usually have trouble seeing, but the key factor here is that if we were to describe how frequent each number in `nums` is, no frequency would ever be greater than `N`, where `N` is the size of `nums`. 

What I mean is, given `nums` looks like this: 

`[1, 1, 1, 1, 1]`, where the length of `nums` is `5`, the frequency of `1` is `5`, and that's the most frequent any element could ever be. 

So since we have a nice upper-bound on the quality (frequency) of the items in the input array, we can use that to our advantage, and leverage it with bucket-sorting. 

What we'll do is loop through the array, which should take us O(n) time, and count the frequency of each number in the array. We'll store these frequencies in a hashmap, where the keys of the hashmap are the numbers, and the values are their frequencies (which will increase each time we see them). 

Then, we'll set up an array for the bucket sorting. That array will be equal to `nums.length + 1` (we need to do this in JavaScript because arrays are 0-indexed, and if we want a bucket to easily represent the frequency of an item like the example above, it's easier to work with an array that allows us to 1-index). 

Then we'll fill that array up with empty subarrays. We'll iterate through the hashmap that tells us the frequencies of each number. When we find the frequency of a number, we'll push it into the subarray in the bucket slot that corresponds to its frequency.

So again, to go back to the `[1, 1, 1, 1, 1]` example, we'll end up with a hashmap that looks like this: 

```js
const frequencies = { '1': 5 }
```

And we'll have a buckets array that looks like this: 

```js
const buckets = [[], [], [], [], [], [1]]
```

Again, notice that buckets has 6 sub-arrays. But we'll basically never use the first one, because there's no use to use keeping track of the infinite number of integers that appear `0` times in the array, haha. 

With our buckets full up, we'll work backwards. For any subarray that has items, we'll push those items into some result array. Every time we push an item into the result array, we'll decrement down some counter from `k` to `0`. Once that counter hits `0`, we can stop, because our result array will have `k` items in it. 

Here's how to do that in JavaScript: 

```js
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var topKFrequent = function(nums, k) {
    // Set up an object to keep track of the frequencies of each number in nums
    const frequencies = {}
    
    // Loop through nums and store the frequency of each item in frequencies.
    for (const num of nums) {
        frequencies[num] = frequencies[num] + 1 || 1
    }
    
    // Next, we create our array of buckets. It needs to be length nums.length + 1,
    // since we don't want to track the integers with `0` frequency, and 
    // it'll be more convenient to be able to 1-index the rest of them.
    //
    // Fill that array with empty sub-arrays.
    // We need to do it this way because JavaScript will pass-by-reference
    // the sub-arrays. 
    // https://stackoverflow.com/questions/41121982/strange-behavior-of-an-array-filled-by-array-prototype-fill
    const buckets = Array.from( new Array(nums.length + 1), function() { return []; } );
    
    // Then go through all the key/value pairs in frequencies, 
    // and for every frequency, push its key into the corresponding bucket.
    for (const key in frequencies) {
        const frequency = frequencies[key]
        // The keys will have been cast into strings,
        // so let's cast them into integers for our final result.
        const number = parseInt(key)
        buckets[frequency].push(number)
    }
    
    // Finally, set up a result array,
    // and walk backwards through our buckets, 
    // pushing the subarrays into the result array.
    // 
    // We will return just the first k elements of that array
    const result = []
    for (let i=buckets.length - 1; i>= 0; i--) {
        const bucket = buckets[i]
        
        for (let j=0; j < bucket.length; j++) {
            result.push(bucket[j])
        }
    }
        
    // Return a slice from 0 to k for the kth elements.
    // We could have instead stopped building this array when it was at length of k,
    // but personally I don't think the code is as readable with that condition,
    // and this meets the big-O runtime we're trying for.
    return result.slice(0, k)
};
```
