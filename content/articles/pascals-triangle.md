---
title: Pascal's Triangle
description: Given an integer numRows, return the first numRows of Pascal's triangle.
slug: pascals-triangle
---

## Description

Given an integer numRows, return the first numRows of Pascal's triangle.

In Pascal's triangle, each number is the sum of the two numbers directly above it as shown:

[!Pascal's Triangle](/images/PascalTriangleAnimated2.gif)

## Examples

**Example 1**

Input: numRows = 5
Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]

**Example 2**

Input: numRows = 5
Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]

## Explanation

This is a dynamic programming problem, which means it's a type of problem where the solution is comprised of solutions to smaller sub problems.

In this instance, the overall solution is to generate an array of arrays. We'll use a technique called "bottom up tabulation" from dyanmic programming. The "tabulation" comes from the idea that every array we need to fill in the triangle can be created from the prior arrays - assuming we have two starting pieces of information: we need to start with the first and second row. 

Fortunately, we know what those rows should be: `[1]` and `[1, 1]` respectively. 

So to generate our triangle, we can do the following: 

1. Start with an empy array, call it `result`
1. Iterate `i` up to `numRows`.
1. When `i` is `0`, append `[1]` to `result`
1. When `i` is `1`, append `[1, 1]` to `result`
1. For every other value of `i`, create a new `subArray` constant
  1. Iterate from 0 to `i` with a new iterator, `j`
  1. If `j` is `0` or the last item in the subArray, `i`, it should be `1`
  1. Otherwise, `subArray[j]` should be `result[i-1][j-1] + result[i-1][j]`, meaning it is the sum of its two "parent" elements in the row above it
  1. Once we've filled in the `subArray`, we can append it to `result` and move on.
1. After all this iteration, we return `result`.

## Code

```js
/**
 * @param {number} numRows
 * @return {number[][]}
 */
var generate = function(numRows) {
  // Set up a result array to store the generated sub-arrays
  const result = []

  // Iterate over the `numRows`
  for (i=0; i<numRows; i++) { 
    // If i === 0, append [1] to result
    // If i === 1, append [1, 1] to result
    if (i === 0) {
      result.push([1])
    } else if (i === 1) {
      result.push([1,1])
    } else {
      // Otherwise, create a new sub-array
      const subArray = []

      // This subarray can be filled by iterating as many times as i
      for (j=0; j<=i; j++) {
        // If j === 0 or j === i - 1, append a 1 to the subarray
        if (j === 0 || j === i) {
          subArray.push(1)
        } else {
          // Otherwise, we want to fill the subarray with the sum of the numbers at position in j-1 and j from the row above this subArray
          const numberToPush = result[i-1][j-1] + result[i-1][j]
          subArray.push(numberToPush)
        }
      }
      // Finally, push this subArray onto the result
      result.push(subArray)
    }
  } 
  
  // Return the result
  return result
};
```
