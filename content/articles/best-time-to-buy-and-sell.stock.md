---
title: Best Time to Buy and Sell Stock
description: Maximize your profit by choosing a single day to buy one stock and choosing a different day in the future to sell that stock.
slug: best-time-to-buy-and-sell-stock
---

## Description 

You are given an array prices where prices[i] is the price of a given stock on the ith day.

You want to maximize your profit by choosing a single day to buy one stock and choosing a different day in the future to sell that stock.

Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0.

### Examples

**Example 1**

Input: prices = [7,1,5,3,6,4]
Output: 5
Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
Note that buying on day 2 and selling on day 1 is not allowed because you must buy before you sell.

**Example 2**

Input: prices = [7,6,4,3,1]
Output: 0
Explanation: In this case, no transactions are done and the max profit = 0.

## Solution 

This problem can be solved using a sliding window/two-pointer technique. The reason that works is that we've got an array-based problem statement, and we want to calculate information about certain sub-sections of the array. A fast way to do this, in linear time, is to use two pointers that move through the set of data.

We'll track a few things:

1. We'll track a maximum profit. That will start at 0, so we can have a good default value in case there are no profits available (in the case that the stock price only goes down)
1. We'll track a left pointer. The left pointer will start at the beginning of the array.
1. We'll track a right pointer. The right pointer will start at the beginning of the array + 1 - just after the left pointer.

At each step, we'll subtract the value at the left pointer position from the right pointer position. If that value is greater than the current max, we'll track it as the new max. In the case where the value is greater than the current max, we'll increment the right pointer by one to see if there's a better profit available. The left pointer will stay in the same place.

If the value isn't greater than the max, but is positive, we'll still move the right pointer, just to see if there are better days in the future to sell.

If, when we subtract the left pointer from the right pointer, there's a negative value, it means that the value at the left can't be the minimum price at all. In fact, it means that the value at the right pointer is our new lowest. So we update the left pointer to the right pointer position, and increment the right pointer again.

We'll repeat this until the right pointer has exceeded the length of the array, at which point, we return the max.


## Code

```js
/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function(prices) {
    // Set up variables to track the max profit we've seen (init at 0),
    // the left pointer (init at day 0)
    // and the right pointer (init at day 1, since we can't buy/sell on the same day)
    let max = 0
    let left = 0
    let right = 1
    
    // Keep incrementing the right pointer until we hit the end of the prices array.
    while (right < prices.length) {

        // The profit on any given day is the difference between the sell date (represented by right pointer)
        // and buy date (represented by the left pointer)
        const profit = prices[right] - prices[left]
        
        // If we can get a profit higher than we've seen before, set it as our maximum,
        // and we'll keep going.
        if (profit > max) {
            max = profit
        }
        
        // If today would be a loss, it means two things: 
        // 1. We don't have a new maximum.
        // 2. We just found a day on which we could buy at a lower price than before, so we should buy now.
        // 
        // The implication of point two is that we move our left pointer (again, as a sliding window) to be the current day,
        // as represented by the right pointer.
        if (profit < 0) {
            left = right
        }
        
        // Increment the right pointer to keep searching.
        right += 1
    }
    
    // This will return 0 if we had no possible profit, since we initialized at 0,
    // or the maximum profit possible.
    return max
};
```
