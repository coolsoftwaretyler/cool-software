---
title: Longest Consecutive Sequence
description: Given an unsorted array of integers nums, return the length of the longest consecutive elements sequence.
slug: longest-consecutive-sequence
link: https://leetcode.com/problems/longest-consecutive-sequence/
---

Given an unsorted array of integers nums, return the length of the longest consecutive elements sequence.

You must write an algorithm that runs in O(n) time.

 

Example 1:

Input: nums = [100,4,200,1,3,2]
Output: 4
Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.
Example 2:

Input: nums = [0,3,7,2,5,8,4,6,0,1]
Output: 9
 

Constraints:

0 <= nums.length <= 105
-109 <= nums[i] <= 109


## Solution

We can solve this problem using a hash map and a clever observation (as is often the case with array-based problems that are unsorted). 

First, we should consider how we would identify and count the length of a consecutive sequence: we would want to start at the lowest number in that sequence. Otherwise, we would double-count subsequences. In the example `[100,4,200,1,3,2]`, we would only want to start at the numbers `100`, `200`, and `1` - because starting at `4`, `3`, or `2` would give us sequences that are guaranteed to be longer (since we know we have `1` in the array). 

So if we wanted to ask, for each number in an array, if it's the lowest in a possible subsequence, all we have to check is whether or not the array has `number - 1` in it. 

To check something like that, we should use a hashmap (or a set), because lookups in a hashmap take constant time. 

Once we figure out what numbers are candidates to *begin* sequences, we can construct the sequences in a similar way: just keep checking `number + 1` as many times as we can until we run out of the incremental values - and keep track of the longest sequence we see at each step along the way. 

So to summarize, we want to: 

1. Push all the numbers into a hashmap or set
1. Do a second loop through the input array, and find possible sequence-starters, by checking if the hashmap has `number - 1`. 
1. If we don't see `number - 1` in the hashmap, start counting up by 1, and incrementing a counter at each step
1. Record the longest counter we see each time we do this process, and return it at the end.

Here's how to write that in JavaScript:

```js
/**
 * @param {number[]} nums
 * @return {number}
 */
var longestConsecutive = function(nums) {
    // Set up a variable to track the longest streak we find,
    // and initialize it at 0
    let longest = 0
    
    // Use a set to store numbers in the nums array, 
    // for constant-time lookup in the second loop.
    // 
    // You could also use a regular JS object for this.
    const numberSet = new Set()
    
    // First loop: add all the numbers in the input array to the numberSet
    for (let number of nums) {
        numberSet.add(number)
    }
    
    // Second loop: 
    // Go through each number. If it's the starting point of a possible sequence,
    // then numberSet won't have that number - 1 in it.
    //
    // For each number that's a starting point, set up a currentStreak,
    // and a variable to track incrementing numbers in that possible sequence.
    // While the numberSet has the current number + 1 in it, keep incrementing the num/streak
    // Finally, for each streak, see if it's the longest streak we've seen.
    for (let number of nums) {
        // If numberset doesn't have number - 1, 
        // then this number could be the beginning of a sequence.
        if (!numberSet.has(number - 1)) {
            // Start current at the number,
            // and currentStreak at 1
            let current = number
            let currentStreak = 1
            
            // While the numberSet has current + 1, 
            // increment current and the streak.
            while (numberSet.has(current + 1)) {
                current++
                currentStreak++
            }
            
            // Once we run out of incremental known numbers,
            // set up the longest.
            longest = Math.max(currentStreak, longest)
        }
    }
    
    // Return the longest we saw.
    return longest
};
```
